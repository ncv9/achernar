//! Contains definitions for finite-state transducers.

use crate::{automaton::Automaton, base::Alphabet};

pub trait Transducer<Sigma: Alphabet, Gamma: Alphabet>:
    Automaton<(Option<Sigma>, Option<Gamma>)>
{
    //
}

impl<Sigma: Alphabet, Gamma: Alphabet, A: Automaton<(Option<Sigma>, Option<Gamma>)>>
    Transducer<Sigma, Gamma> for A
{
}

pub trait SameLengthTransducer<Sigma: Alphabet, Gamma: Alphabet>:
    Automaton<(Sigma, Gamma)>
{
    //
}

impl<Sigma: Alphabet, Gamma: Alphabet, A: Automaton<(Sigma, Gamma)>>
    SameLengthTransducer<Sigma, Gamma> for A
{
}
