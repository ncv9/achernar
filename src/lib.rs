//! Handles finite state automata and transducers on arbitrary alphabets.

#![allow(incomplete_features)]
#![feature(return_position_impl_trait_in_trait)] // God damn it.
#![feature(impl_trait_projections)]

pub mod automaton;
pub mod base;
pub mod transducer;

#[cfg(test)]
mod tests {
    use crate::automaton::{matches, Automaton, Cartesian, Concat, Epsilon, Literal, Star, Union};

    #[test]
    fn match_literal_state() {
        let automaton = Literal(b"code");
        assert!(matches(&automaton, b"code".iter().copied()));
        assert!(!matches(&automaton, b"dog".iter().copied()));
        assert!(!matches(&automaton, b"cod".iter().copied()));
        assert!(!matches(&automaton, b"codes".iter().copied()));
    }

    #[test]
    fn match_union() {
        let automaton = Union(Literal(b"voepfxo"), Literal(b"xbsio"));
        assert!(matches(&automaton, b"voepfxo".iter().copied()));
        assert!(matches(&automaton, b"xbsio".iter().copied()));
        assert!(!matches(&automaton, b"ha znxs".iter().copied()));
        assert!(!matches(&automaton, b"voepfxos".iter().copied()));
        assert!(!matches(&automaton, b"xbs".iter().copied()));
    }

    #[test]
    fn match_concat() {
        let automaton = Concat(
            Union(Literal(b"a"), Literal(b"b")),
            Union(Literal(b"c"), Literal(b"d")),
        );
        assert!(matches(&automaton, b"ac".iter().copied()));
        assert!(matches(&automaton, b"ad".iter().copied()));
        assert!(matches(&automaton, b"bc".iter().copied()));
        assert!(matches(&automaton, b"bd".iter().copied()));
        assert!(!matches(&automaton, b"".iter().copied()));
        assert!(!matches(&automaton, b"aa".iter().copied()));
        assert!(!matches(&automaton, b"aab".iter().copied()));
        assert!(!matches(&automaton, b"abb".iter().copied()));
    }

    #[test]
    fn match_concat2() {
        let automaton = Concat(Literal(b"a"), Union(Literal(b"b"), Epsilon));
        assert!(matches(&automaton, b"a".iter().copied()));
        assert!(matches(&automaton, b"ab".iter().copied()));
        assert!(!matches(&automaton, b"".iter().copied()));
        assert!(!matches(&automaton, b"b".iter().copied()));
        assert!(!matches(&automaton, b"aab".iter().copied()));
        assert!(!matches(&automaton, b"abb".iter().copied()));
    }

    #[test]
    fn match_star() {
        let automaton = Star(Concat(Literal(b"a"), Union(Literal(b"b"), Epsilon)));
        assert!(matches(&automaton, b"".iter().copied()));
        assert!(matches(&automaton, b"a".iter().copied()));
        assert!(matches(&automaton, b"ab".iter().copied()));
        assert!(matches(&automaton, b"aab".iter().copied()));
        assert!(matches(&automaton, b"aaaa".iter().copied()));
        assert!(matches(&automaton, b"abab".iter().copied()));
        assert!(!matches(&automaton, b"abb".iter().copied()));
        assert!(!matches(&automaton, b"abc".iter().copied()));
    }

    #[test]
    fn match_star2() {
        let automaton = Star(Literal(b"a"));
        assert!(matches(&automaton, b"".iter().copied()));
        assert!(matches(&automaton, b"a".iter().copied()));
        assert!(matches(&automaton, b"aa".iter().copied()));
        assert!(matches(&automaton, b"aaa".iter().copied()));
        assert!(matches(&automaton, b"aaaa".iter().copied()));
        assert!(!matches(&automaton, b"ab".iter().copied()));
        assert!(!matches(&automaton, b"b".iter().copied()));
    }

    #[test]
    fn match_expand() {
        let automaton = Literal(b"code").expand::<u16>();
        assert!(matches(&automaton, [99, 111, 100, 101]));
        assert!(!matches(&automaton, [99, 111, 100]));
    }

    #[test]
    fn match_cartesian() {
        let automaton = Cartesian(
            Star(Literal(b"a")),
            Concat(Literal(b"a"), Union(Literal(b"b"), Epsilon)),
        );
        assert!(matches(&automaton, [(b'a', b'a')]));
        assert!(matches(&automaton, [(b'a', b'a'), (b'a', b'b')]));
        assert!(!matches(&automaton, [(b'a', b'b')]));
        assert!(!matches(&automaton, [(b'a', b'a'), (b'a', b'c')]));
        assert!(!matches(&automaton, [(b'c', b'a'), (b'a', b'b')]));
        assert!(!matches(&automaton, [(b'a', b'a'), (b'b', b'a')]));
    }
}
