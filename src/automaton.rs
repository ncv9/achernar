//! Contains definitions for finite-state automata.

use std::{collections::BTreeSet, fmt::Debug, iter, marker::PhantomData};

use either::Either;
use itertools::Itertools;

use crate::base::{Alphabet, Subset, Superset};

/// A trait describing a finite-state automaton.
pub trait Automaton<Sigma: Alphabet> {
    /// The type used to carry the state.
    type State: Eq + Ord + Debug + Clone + 'static;

    /// Returns an iterator of the start states.
    fn start(&self) -> impl Iterator<Item = Self::State> + Clone + '_;

    /// Returns whether the given state is one of the accepted states.
    fn accepts(&self, state: &Self::State) -> bool;

    /// Returns the state to transition to from a given state and symbol.
    fn next_state(
        &self,
        state: Self::State,
        symbol: Sigma,
    ) -> impl Iterator<Item = Self::State> + Clone + '_;

    /// Returns whether this automaton can match any strings starting from
    /// the given state.
    ///
    /// It is always legal to return `true` from this function.
    fn can_match(&self, _state: &Self::State) -> bool {
        true
    }

    /// Returns whether this automaton will match all strings starting from
    /// the given state.
    ///
    /// It is always legal to return `false` from this function.
    fn must_match(&self, _state: &Self::State) -> bool {
        false
    }

    /// Expands the alphabet for an automaton.
    fn expand<Sigma2: Alphabet>(self) -> Expand<Sigma2, Sigma, Self>
    where
        Sigma2: Superset<Sigma>,
        Self: Sized,
    {
        Expand::new(self)
    }
}

/// Returns whether an automaton accepts a given sequence of symbols.
pub fn matches<Sigma: Alphabet, A: Automaton<Sigma>>(
    automaton: &A,
    input: impl IntoIterator<Item = Sigma>,
) -> bool {
    let mut state: Vec<_> = automaton.start().collect();
    for symbol in input {
        if !state.iter().any(|s| automaton.can_match(s)) {
            return false;
        }
        if state.iter().any(|s| automaton.must_match(s)) {
            return true;
        }
        state = state
            .into_iter()
            .flat_map(|s| automaton.next_state(s, symbol.clone()))
            .collect();
        state.sort();
        state.dedup();
    }
    state.iter().any(|s| automaton.accepts(s))
}

impl<'a, Sigma: Alphabet, A: Automaton<Sigma>> Automaton<Sigma> for &'a A {
    type State = A::State;

    fn start(&self) -> impl Iterator<Item = Self::State> + Clone + '_ {
        A::start(self)
    }

    fn accepts(&self, state: &Self::State) -> bool {
        A::accepts(self, state)
    }

    fn next_state(
        &self,
        state: Self::State,
        symbol: Sigma,
    ) -> impl Iterator<Item = Self::State> + Clone + '_ {
        A::next_state(self, state, symbol)
    }

    fn can_match(&self, state: &Self::State) -> bool {
        A::can_match(self, state)
    }

    fn must_match(&self, state: &Self::State) -> bool {
        A::must_match(self, state)
    }
}

/// Expands the alphabet for an automaton.
#[derive(Debug, Clone, Copy)]
pub struct Expand<Sigma2, Sigma, A>(A, PhantomData<fn(Sigma) -> Sigma2>);

impl<Sigma2, Sigma, A> Expand<Sigma2, Sigma, A> {
    pub fn new(a: A) -> Self {
        Self(a, PhantomData)
    }
}

impl<Sigma: Alphabet, Sigma2: Alphabet, A: Automaton<Sigma>> Automaton<Sigma2>
    for Expand<Sigma2, Sigma, A>
where
    Sigma2: Superset<Sigma>,
{
    type State = A::State;

    fn start(&self) -> impl Iterator<Item = Self::State> + Clone + '_ {
        A::start(&self.0)
    }

    fn accepts(&self, state: &Self::State) -> bool {
        A::accepts(&self.0, state)
    }

    fn next_state(
        &self,
        state: Self::State,
        symbol: Sigma2,
    ) -> impl Iterator<Item = Self::State> + Clone + '_ {
        match Subset::try_from(symbol) {
            Some(symbol) => Either::Left(A::next_state(&self.0, state, symbol)),
            None => Either::Right(iter::empty()),
        }
    }

    fn can_match(&self, state: &Self::State) -> bool {
        A::can_match(&self.0, state)
    }

    fn must_match(&self, state: &Self::State) -> bool {
        A::must_match(&self.0, state)
    }
}

/// An automaton that accepts only the empty sequence.
#[derive(Debug, Clone, Copy)]
pub struct Epsilon;

impl<Sigma: Alphabet> Automaton<Sigma> for Epsilon {
    type State = ();

    fn start(&self) -> impl Iterator<Item = Self::State> + Clone + '_ {
        iter::once(())
    }

    fn accepts(&self, _state: &Self::State) -> bool {
        true
    }

    fn next_state(
        &self,
        _state: Self::State,
        _symbol: Sigma,
    ) -> impl Iterator<Item = Self::State> + Clone + '_ {
        iter::empty()
    }
}

/// An automaton matching a literal sequence of symbols.
#[derive(Debug, Clone, Copy)]
pub struct Literal<'a, Sigma>(pub &'a [Sigma]);

impl<'a, Sigma: Alphabet> Automaton<Sigma> for Literal<'a, Sigma> {
    // States 0..=n are the fencepost states when no mismatching characters
    // have been encountered so far.
    type State = usize;

    fn start(&self) -> impl Iterator<Item = Self::State> + Clone + '_ {
        iter::once(0)
    }

    fn accepts(&self, state: &Self::State) -> bool {
        *state == self.0.len()
    }

    fn next_state(
        &self,
        state: Self::State,
        symbol: Sigma,
    ) -> impl Iterator<Item = Self::State> + Clone + '_ {
        let new_state = if state < self.0.len() {
            if symbol == self.0[state] {
                Some(state + 1)
            } else {
                None
            }
        } else {
            None
        };
        new_state.into_iter()
    }
}

/// A union of two regular languages: accepts strings matching either one of
/// the underlying languages.
#[derive(Debug, Clone, Copy)]
pub struct Union<A, B>(pub A, pub B);

impl<Sigma: Alphabet, A: Automaton<Sigma>, B: Automaton<Sigma>> Automaton<Sigma> for Union<A, B> {
    type State = Either<A::State, B::State>;

    fn start(&self) -> impl Iterator<Item = Self::State> + Clone + '_ {
        self.0
            .start()
            .map(Either::Left)
            .chain(self.1.start().map(Either::Right))
    }

    fn accepts(&self, state: &Self::State) -> bool {
        match state {
            Either::Left(s) => self.0.accepts(s),
            Either::Right(s) => self.1.accepts(s),
        }
    }

    fn next_state(
        &self,
        state: Self::State,
        symbol: Sigma,
    ) -> impl Iterator<Item = Self::State> + Clone + '_ {
        match state {
            Either::Left(s) => Either::Left(self.0.next_state(s, symbol).map(Either::Left)),
            Either::Right(s) => Either::Right(self.1.next_state(s, symbol).map(Either::Right)),
        }
    }

    fn can_match(&self, state: &Self::State) -> bool {
        match state {
            Either::Left(s) => self.0.can_match(s),
            Either::Right(s) => self.1.can_match(s),
        }
    }

    fn must_match(&self, state: &Self::State) -> bool {
        match state {
            Either::Left(s) => self.0.must_match(s),
            Either::Right(s) => self.1.must_match(s),
        }
    }
}

/// The concatenation of two regular languages.
#[derive(Debug, Clone, Copy)]
pub struct Concat<A, B>(pub A, pub B);

impl<Sigma: Alphabet, A: Automaton<Sigma>, B: Automaton<Sigma>> Automaton<Sigma> for Concat<A, B> {
    type State = Either<A::State, B::State>;

    fn start(&self) -> impl Iterator<Item = Self::State> + Clone + '_ {
        self.0.start().map(Either::Left)
    }

    fn accepts(&self, state: &Self::State) -> bool {
        match state {
            Either::Left(_) => false,
            Either::Right(s) => self.1.accepts(s),
        }
    }

    fn next_state(
        &self,
        state: Self::State,
        symbol: Sigma,
    ) -> impl Iterator<Item = Self::State> + Clone + '_ {
        match state {
            Either::Left(s) => {
                let mut visited_accepted = false;
                let mut v: Vec<_> = self
                    .0
                    .next_state(s, symbol)
                    .inspect(|s| {
                        if self.0.accepts(s) {
                            visited_accepted = true;
                        }
                    })
                    .map(Either::Left)
                    .collect();
                if visited_accepted {
                    v.extend(self.1.start().map(Either::Right));
                }
                Either::Left(v.into_iter())
            }
            Either::Right(s) => Either::Right(self.1.next_state(s, symbol).map(Either::Right)),
        }
    }

    fn can_match(&self, state: &Self::State) -> bool {
        match state {
            Either::Left(s) => self.0.can_match(s),
            Either::Right(s) => self.1.can_match(s),
        }
    }

    fn must_match(&self, state: &Self::State) -> bool {
        match state {
            Either::Left(_) => false,
            Either::Right(s) => self.1.must_match(s),
        }
    }
}

pub type Opt<A> = Union<A, Epsilon>;

/// The Kleene star closure of a regular language.
/// Matches the underlying language zero or more times.
#[derive(Debug, Clone, Copy)]
pub struct Star<A>(pub A);

impl<Sigma: Alphabet, A: Automaton<Sigma>> Automaton<Sigma> for Star<A> {
    type State = (A::State, bool);

    fn start(&self) -> impl Iterator<Item = Self::State> + Clone + '_ {
        self.0.start().map(|x| (x, true))
    }

    fn accepts(&self, state: &Self::State) -> bool {
        state.1 || self.0.accepts(&state.0)
    }

    fn next_state(
        &self,
        state: Self::State,
        symbol: Sigma,
    ) -> impl Iterator<Item = Self::State> + Clone + '_ {
        let mut visited_accepted = false;
        let mut v: Vec<_> = self
            .0
            .next_state(state.0, symbol)
            .inspect(|s| {
                if self.0.accepts(s) {
                    visited_accepted = true;
                }
            })
            .map(|x| (x, false))
            .collect();
        if visited_accepted {
            v.extend(self.start());
        }
        v.into_iter()
    }

    fn can_match(&self, state: &Self::State) -> bool {
        state.1 || self.0.can_match(&state.0)
    }

    fn must_match(&self, state: &Self::State) -> bool {
        self.0.must_match(&state.0)
    }
}

/// The Cartesian product of two regular languages.
///
/// Note that this is an automaton over the cartesian product of the
/// constituent automata’s alphabets, so that it can only match pairs of
/// strings with the same length. For example, it can determine whether
/// `(abc, def)` (represented as `[(a, d), (b, e), (c, f)]`) matches, but it
/// makes no sense for it to determine whether `(abc, de)` does.
#[derive(Debug, Clone, Copy)]
pub struct Cartesian<A, B>(pub A, pub B);

impl<Sigma: Alphabet, Gamma: Alphabet, A: Automaton<Sigma>, B: Automaton<Gamma>>
    Automaton<(Sigma, Gamma)> for Cartesian<A, B>
{
    type State = (A::State, B::State);

    fn start(&self) -> impl Iterator<Item = Self::State> + Clone + '_ {
        self.0.start().cartesian_product(self.1.start())
    }

    fn accepts(&self, state: &Self::State) -> bool {
        self.0.accepts(&state.0) && self.1.accepts(&state.1)
    }

    fn next_state(
        &self,
        state: Self::State,
        symbol: (Sigma, Gamma),
    ) -> impl Iterator<Item = Self::State> + Clone + '_ {
        self.0
            .next_state(state.0, symbol.0)
            .cartesian_product(self.1.next_state(state.1, symbol.1))
    }

    fn can_match(&self, _state: &Self::State) -> bool {
        true
    }

    fn must_match(&self, _state: &Self::State) -> bool {
        false
    }
}

/// Return an equivalent automaton that has epsilon transitions.
#[derive(Debug, Clone, Copy)]
pub struct Epsilonize<A>(pub A);
impl<Sigma: Alphabet, A: Automaton<Sigma>> Automaton<Option<Sigma>> for Epsilonize<A> {
    type State = A::State;

    fn start(&self) -> impl Iterator<Item = Self::State> + Clone + '_ {
        self.0.start()
    }

    fn accepts(&self, state: &Self::State) -> bool {
        self.0.accepts(state)
    }

    fn next_state(
        &self,
        state: Self::State,
        symbol: Option<Sigma>,
    ) -> impl Iterator<Item = Self::State> + Clone + '_ {
        match symbol {
            Some(symbol) => Either::Left(self.0.next_state(state, symbol)),
            None => Either::Right(iter::once(state)),
        }
    }

    fn must_match(&self, state: &Self::State) -> bool {
        self.0.must_match(state)
    }
}

/// Return an automaton that folds epsilon transitions.
// FIXME?: this should probably be part of traversal
#[derive(Debug, Clone, Copy)]
pub struct EpsClosure<A>(pub A);

impl<Sigma: Alphabet, A: Automaton<Option<Sigma>>> Automaton<Sigma> for EpsClosure<A> {
    type State = A::State;

    fn start(&self) -> impl Iterator<Item = Self::State> + Clone + '_ {
        let mut ec: BTreeSet<Self::State> = BTreeSet::new();
        let mut work: Vec<_> = self.0.start().collect();
        while let Some(s) = work.pop() {
            if ec.insert(s.clone()) {
                work.extend(self.0.next_state(s, None));
            }
        }
        work.clear();
        work.extend(ec);
        work.into_iter()
    }

    fn accepts(&self, state: &Self::State) -> bool {
        self.0.accepts(state)
    }

    fn next_state(
        &self,
        state: Self::State,
        symbol: Sigma,
    ) -> impl Iterator<Item = Self::State> + Clone + '_ {
        let mut ec: BTreeSet<Self::State> = BTreeSet::new();
        let mut work: Vec<_> = self.0.next_state(state, Some(symbol)).collect();
        while let Some(s) = work.pop() {
            if ec.insert(s.clone()) {
                work.extend(self.0.next_state(s, None));
            }
        }
        work.clear();
        work.extend(ec);
        work.into_iter()
    }

    fn can_match(&self, state: &Self::State) -> bool {
        self.0.can_match(state)
    }

    fn must_match(&self, state: &Self::State) -> bool {
        self.0.must_match(state)
    }
}
