//! Basic trait definitions.

use std::fmt::Debug;

use exhaust::Exhaust;

/// Describes a symbol from a finite list.
pub trait Alphabet: Exhaust + Eq + Debug {
    // ???
}

impl Alphabet for u8 {}
impl Alphabet for u16 {}

impl<S, T> Alphabet for (S, T)
where
    S: Alphabet,
    T: Alphabet,
{
}
impl<S> Alphabet for Option<S> where S: Alphabet {}

pub trait Superset<A> {
    fn from(a: A) -> Self;
    fn try_into(self) -> Option<A>;
}

// FIXME: this implementation will conflict with the one for pairs

// impl<A> Superset<A> for A {
//     fn from(a: A) -> Self {
//         a
//     }

//     fn try_into(self) -> Option<A> {
//         Some(self)
//     }
// }

macro_rules! impl_superset {
    ($a:ty, $b:ty) => {
        impl Superset<$a> for $b {
            fn from(a: $a) -> Self {
                From::from(a)
            }

            fn try_into(self) -> Option<$a> {
                TryFrom::try_from(self).ok()
            }
        }
    };
}

impl_superset!(u8, u8);
impl_superset!(u8, u16);
impl_superset!(u8, u32);
impl_superset!(u8, u64);
impl_superset!(u8, u128);
impl_superset!(u8, usize);
impl_superset!(u16, u16);
impl_superset!(u16, u32);
impl_superset!(u16, u64);
impl_superset!(u16, u128);
impl_superset!(u16, usize);
impl_superset!(u32, u32);
impl_superset!(u32, u64);
impl_superset!(u32, u128);
impl_superset!(u64, u64);
impl_superset!(u64, u128);
impl_superset!(u128, u128);
impl_superset!(u8, i16);
impl_superset!(u8, i32);
impl_superset!(u8, i64);
impl_superset!(u8, i128);
impl_superset!(u8, isize);
impl_superset!(u16, i32);
impl_superset!(u16, i64);
impl_superset!(u16, i128);
impl_superset!(u32, i64);
impl_superset!(u32, i128);
impl_superset!(u64, i128);
impl_superset!(i8, i8);
impl_superset!(i8, i16);
impl_superset!(i8, i32);
impl_superset!(i8, i64);
impl_superset!(i8, i128);
impl_superset!(i8, isize);
impl_superset!(i16, i16);
impl_superset!(i16, i32);
impl_superset!(i16, i64);
impl_superset!(i16, i128);
impl_superset!(i16, isize);
impl_superset!(i32, i32);
impl_superset!(i32, i64);
impl_superset!(i32, i128);
impl_superset!(i64, i64);
impl_superset!(i64, i128);
impl_superset!(i128, i128);
impl_superset!(u8, char);
impl_superset!(char, u32);
impl_superset!(char, char);

impl<A> Superset<A> for Option<A> {
    fn from(a: A) -> Self {
        Some(a)
    }

    fn try_into(self) -> Option<A> {
        self
    }
}

impl<A1, A2, B1, B2> Superset<(A1, B1)> for (A2, B2)
where
    A2: Superset<A1>,
    B2: Superset<B1>,
{
    fn from(a: (A1, B1)) -> Self {
        (Superset::from(a.0), Superset::from(a.1))
    }

    fn try_into(self) -> Option<(A1, B1)> {
        match (Superset::try_into(self.0), Superset::try_into(self.1)) {
            (Some(a), Some(b)) => Some((a, b)),
            _ => None,
        }
    }
}

pub trait Subset<A>
where
    A: Superset<Self>,
    Self: Sized,
{
    fn into(self) -> A {
        A::from(self)
    }
    fn try_from(a: A) -> Option<Self> {
        a.try_into()
    }
}

impl<A, B: Superset<A>> Subset<B> for A {}
